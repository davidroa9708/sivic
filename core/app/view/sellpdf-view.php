<?php
require('../../../plugins/fpdf/fpdf.php');
include('../../../core/controller/Database.php');
include('number_word.php');

$id = $_GET['id'];
$nw = new NumberWord();
$base = new Database();
$con = $base->connect();
$sql = 'select  s.created_at,
                s.observation,
                s.discount,
                s.iva,
                s.ico,
                s.subtotal,
                s.total,
                s.method_pay,
                p.name as client,
                p.dni,
                p.address1,
                p.phone1,
                p.city,
                u.name as seller
            from sell s
            inner join person p on p.id = s.person_id
            inner join user u on u.id = s.user_id
            where s.id = '.$id;
            
$query = $con->query($sql);
$sell = $query->fetch_array();

$sql_detail = 'select  *
            from operation o
            inner join product p on p.id = o.product_id
            where o.sell_id = '.$id;
$query_detail = $con->query($sql_detail);

if(strlen($sell['total']) > 7 ){
    $aux = strlen($sell['total']) - 7;
    if(intval(substr($sell['total'], $aux)) > 0){
        $str = ' PESOS';
    } else {
        $str = ' DE PESOS';
    }
} else {
    $str = ' PESOS';
}

$created_at = date("Y-m-d",strtotime($sell['created_at']."+ 1 year"));
$expiration_date = date("Y-m-d",strtotime($sell['created_at']."+ 1 year 1 month"));

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetAutoPageBreak(false);
$pdf->SetFont('Arial','',10);
$pdf->Image('../../../storage/img/imm.png',6,7);
$pdf->Image('../../../storage/img/bps.png',140,5,65,40);
$pdf->Cell(120,55,utf8_decode('Régimen de IVA: Común - No Gran Contribuyente - No Autorretenedor de Renta'),0,0,'C');
$pdf->SetXY(75,12);
$pdf->Cell(1,60,utf8_decode('Act. Económica Principal: 7020'),0,0,'C');
$pdf->SetFont('Arial','',9);
$pdf->SetXY(100,20);
$pdf->Cell(1,60,utf8_decode('Factura de Venta Nro:'.$id.' - Fecha Elaboración: '.$created_at.' - Fecha de Vencimiento: '.$expiration_date.'  -  Forma de Pago: '.$sell['method_pay']),0,0,'C');

$pdf->SetXY(7,30);
$pdf->Cell(1,60,utf8_decode('Cliente: '.$sell['client']),0,0,'L');

$pdf->SetXY(90,30);
$pdf->Cell(1,60,utf8_decode('Dirección: '.$sell['address1']),0,0,'L');

$pdf->SetXY(7,37);
$pdf->Cell(5,60,utf8_decode('Nit. o C.C.: '.$sell['dni']),0,0,'L');

$pdf->SetXY(75,37);
$pdf->Cell(1,60,utf8_decode('Ciudad: '.$sell['city']),0,0,'L');

$pdf->SetXY(135,37);
$pdf->Cell(1,60,utf8_decode('Teléfono: '.$sell['phone1']),0,0,'L');

$pdf->SetXY(7,50);
$pdf->Cell(1,60,utf8_decode('Vendedor:'.$sell['seller']),0,0,'L');

$pdf->SetXY(8,60);
$pdf->Cell(30,60,utf8_decode('Item'),0,0,'L');
$pdf->Cell(70,60,utf8_decode('Descripción'),0,0,'L');
$pdf->Cell(15,60,utf8_decode('UM'),0,0,'L');
$pdf->Cell(30,60,utf8_decode('Cantidad'),0,0,'L');
$pdf->Cell(30,60,utf8_decode('Vlr. Unitario'),0,0,'L');
$pdf->Cell(30,60,utf8_decode('Vlr. Total'),0,0,'L');
$y = 67;
$y2 = 95;

while($sell_detail = $query_detail->fetch_array()){
    $pdf->SetXY(7,$y);
    $pdf->Cell(30,60,utf8_decode($sell_detail['name']),0,0,'L');
    $pdf->SetXY(39,$y2);
    $pdf->MultiCell(60,4,utf8_decode($sell_detail['description']),'','L', false);
    $pdf->SetXY(96,$y);
    $pdf->Cell(20,60,utf8_decode($sell_detail['unit']),0,0,'C');
    $pdf->Cell(30,60,utf8_decode($sell_detail['q']),0,0,'C');
    $pdf->Cell(30,60,number_format($sell_detail['price_out']),0,0,'C');
    $pdf->Cell(30,60,number_format($sell_detail['price_out']*$sell_detail['q']),0,0,'C');
    $exp = explode("\n", $sell_detail['description']);
    $y_desc = count($exp);
    $y += ($y_desc * 4.2) ;
    $y2 += ($y_desc * 4.2);
}
$y2 += 5;
$y_aux = $y2;
$pdf->SetFont('Arial','B',10);
$pdf->SetXY(7,$y2);
$pdf->MultiCell(140,5,utf8_decode('Observaciones: '.$sell['observation']),'','L', false);
$pdf->SetFont('Arial','',10);
$y2 += 25;
$pdf->SetXY(7,$y2);
$pdf->Cell(5,5,utf8_decode('Firma: ______________________________________________ Sello:'),0,0,'L');
$y2 += 10;
$pdf->SetXY(7,$y2);
$pdf->Cell(1,5,utf8_decode('C.C.'),0,0,'L');


$pdf->SetFont('Arial','B',10);
$pdf->SetXY(160,$y_aux);
$pdf->Cell(1,5,utf8_decode('Subtotal'),0,0,'C');
$pdf->SetXY(190,$y_aux);
$pdf->Cell(1,5,number_format($sell['subtotal']),0,0,'C');
$y_aux += 7;
$pdf->SetXY(160,$y_aux);
$pdf->Cell(1,5,utf8_decode('Descuentos'),0,0,'C');
$pdf->SetXY(190,$y_aux);
$pdf->Cell(1,5,number_format($sell['discount']),0,0,'C');
$y_aux += 7;
$pdf->SetXY(160,$y_aux);
$pdf->Cell(1,5,utf8_decode('IVA'),0,0,'C');
$pdf->SetXY(190,$y_aux);
$pdf->Cell(1,5,number_format($sell['iva']),0,0,'C');
$y_aux += 7;
$pdf->SetXY(160,$y_aux);
$pdf->Cell(1,5,utf8_decode('ICO 8%'),0,0,'C');
$pdf->SetXY(190,$y_aux);
$pdf->Cell(1,5,number_format($sell['ico']),0,0,'C');
$y_aux += 7;
$pdf->SetXY(160,$y_aux);
$pdf->Cell(1,5,utf8_decode('Retenciones'),0,0,'C');
$pdf->SetXY(190,$y_aux);
$pdf->Cell(1,5,number_format('0.00'),0,0,'C');
$y_aux += 7;
$pdf->SetXY(160,$y_aux);
$pdf->Cell(1,5,utf8_decode('Total'),0,0,'C');
$pdf->SetXY(190,$y_aux);
$pdf->Cell(1,5,number_format($sell['total']),0,0,'C');
$y_aux += -15;
$pdf->SetFont('Arial','',10);
$pdf->SetXY(16,$y_aux);
$pdf->Cell(1,60,utf8_decode('Efectivo: '),0,0,'C');
$pdf->SetXY(41,$y_aux);
$pdf->Cell(1,60,number_format(12313100),0,0,'C');

$pdf->SetXY(110,$y_aux);
$pdf->Cell(1,60,utf8_decode('Anticipo: '),0,0,'C');
$pdf->SetXY(131,$y_aux);
$pdf->Cell(1,60,number_format(12313100),0,0,'C');
$y_aux += 7;
$pdf->SetXY(14,$y_aux);
$pdf->Cell(1,60,utf8_decode('Saldo: '),0,0,'C');
$pdf->SetXY(24,$y_aux);
$pdf->Cell(1,60,number_format(0),0,0,'C');
$y_aux += 7;
$pdf->SetXY(7,$y_aux);
$pdf->Cell(1,60,utf8_decode('Son: '.strtoupper($nw->convertirCifrasEnLetras($sell['total'])).$str),0,0,'L');

$y_aux += 6;
$pdf->SetFont('Arial','',8);
$pdf->SetXY(7,$y_aux);
$pdf->Cell(1,60,utf8_decode('El comprador se obliga incondicionalmente a pagar a la orden de INVERSIONES MEJIA Y MEJIA SAS el precio en la forma aquí pactada, pago que se '),0,0,'L');
$y_aux += 3;
$pdf->SetXY(7,$y_aux);
$pdf->Cell(1,60,utf8_decode('efectuará en CHIA, CUNDINAMARCA. En caso de mora, el comprador pagará intereses a la tasa máxima legal permitida en Colombia. '),0,0,'L');
$y_aux += 7;
$pdf->SetXY(7,$y_aux);
$pdf->Cell(1,60,utf8_decode('El comprador tiene derecho a presentar Peticiones, Quejas o Reclamos (PQR) por el incumplimiento de los términos y condiciones de la garantía.'),0,0,'L');
$y_aux += 3;
$pdf->SetXY(7,$y_aux);
$pdf->Cell(1,60,utf8_decode('La Red de Concesionarios y Centros de Servicio Autorizado de Auteco, tiene un responsable local para la atención de estos eventos, a disposición '),0,0,'L');
$y_aux += 3;
$pdf->SetXY(7,$y_aux);
$pdf->Cell(1,60,utf8_decode('del comprador. La presentación de PQRs no tiene que ser personal, ni requiere intervención de abogado. La relación entre las partes, se encuentra'),0,0,'L');
$y_aux += 3;
$pdf->SetXY(7,$y_aux);
$pdf->Cell(1,60,utf8_decode('regulada en el Decreto 3466 de 1982 y en la Circular Única de la SIC.'),0,0,'L');
$y_aux += 5;

$pdf->SetXY(7,$y_aux);
$pdf->Cell(1,60,utf8_decode('Expresamente, a nombre del comprador, manifiesto que he recibido y aceptado esta factura y su contenido y declaro haber recibido a satisfacción la'),0,0,'L');
$y_aux += 3;
$pdf->SetXY(7,$y_aux);
$pdf->Cell(1,60,utf8_decode('mercancía, bienes y servicios que ella cubre, hoy ____'.$created_at.'__________________'),0,0,'L');

$y_aux += 5;
$pdf->SetXY(7,$y_aux);
$pdf->Cell(1,60,utf8_decode('NUMERO DE RESOLUCIÓN: 13028005188188 - 2017-07-24 - HABILITA: MOT 0001-MOT 1000'),0,0,'L');

$pdf->output();
?>